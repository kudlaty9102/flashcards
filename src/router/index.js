import Vue from "vue";
import Router from "vue-router";
import HelloWorld from "@/components/HelloWorld";
import AddWords from "@/components/AddWords";
import WordsList from "@/components/WordsList";
import Login from "@/components/Login";
import Logout from "@/components/Logout";
import AuthGuard from '../store/auth-guard'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/hello",
            name: "Hello",
            component: HelloWorld
        },
        {
            path: "/add",
            name: "AddWords",
            component: AddWords,
            beforeEnter: AuthGuard
        },
        {
            path: "/words",
            name: "WordsList",
            component: WordsList,
            beforeEnter: AuthGuard

        },
        {
            path: "/login",
            name: "Login",
            component: Login
        },
        {
            path: "/logout",
            name: "Logout",
            component: Logout
        }
    ]
});
