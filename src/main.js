import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import router from './router'
import  store  from './store'
import App from './App.vue'
import Vuex from 'vuex'
import {
  Vuetify,
  VApp,
  VCard,
  VList
} from 'vuetify';
import './stylus/main.styl'

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
Vue.use(Vuetify,{
  components: {
    VApp,
    VCard,
    VList
  }
})
Vue.use(Vuex)






