import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
//import { URLSearchParams } from "url";
import * as url from "url";
url.URLSearchParams = URLSearchParams;
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        words: [{ word: "Bad", translations: "Zly" }]
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload;
        }
    },
    actions: {
        logoutUser() {
            localStorage.removeItem("access_token");
        },
        signUserIn(context, payload) {
            payload.password;
            var params = new URLSearchParams();
            params.append("grant_type", "password");
            params.append("username", "admin");
            params.append("password", "adminPassword");
            axios({
                method: "post",
                url: "http://localhost:8090/oauth/token",
                auth: { username: "my-trusted-client", password: "secret" },
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
                },
                data: params
            }).then(function(response) {
                const user = {
                    login: payload.login
                };
                localStorage.setItem("access_token", response.data.access_token);

                context.commit("setUser", user);
            });
        }
    },
    getters: {
        isUserAuthenticated() {
            var token = localStorage.getItem("access_token");
            return token != null;
        },
        user(state) {
            return state.user;
        }
    }
});
