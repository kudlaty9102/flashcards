export default (to, from, next) => {
    if (localStorage.getItem("access_token")) {
        next()
    } else {
        next('/signin')
    }
}